<?php
require_once 'config.php';
$dbs = new PDO($dsn, $username, $password, $options);
$sql3 = 'SELECT  p.*,  c.categoryName FROM products p LEFT JOIN categories c ON p.category_id = c.id GROUP BY p.title, c.categoryName';
$stmt = $dbs->prepare($sql3);
$stmt->execute();
$row_count = $stmt->rowCount();

// Categories_aaray
function categ2array()
{
    global $stmt;
    global $row_count;
    $cat_arr = [];
    if ($row_count >0):
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $cat_arr[$row['id']] = $row;
    }
    return $cat_arr;
    endif;
}

//map tree
function map_tree($dataset)
{
    $tree = [];
    foreach ($dataset as $id => &$node) {
        if (!isset($node['categoryName'])) {
            $tree[$id] = &$node;
        } else {
            $tree[$node['categoryName']]['childs'][$id] = &$node;
        }
    }
    return $tree;
}

$cat_arr = categ2array();
echo '<pre>';
//print_r($cat_arr);
echo '</pre>';
$category_tree = map_tree($cat_arr);
echo '<pre>';
//print_r($category_tree);
echo '</pre>';
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Accordions</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css"/>
    <link rel="stylesheet" href="css/accordion.css">
</head>
<body>
<!-- content starts here -->
<div class="container">
    <h3>Accordion category menu</h3>
    <?php
    if ($category_tree) {
        foreach ($category_tree as $category => $item) {
            ?>
            <button class="accordion"><?php echo $category; ?></button>
            <div class="accordion-content">
                <?php
                if ($item['childs']) {
                    foreach ($item['childs'] as $val) {
                        ?>
                        <p>
                            <?php echo $val["title"]; ?>
                        </p>
                        <?php
                    }
                }
                ?>
            </div>
            <?php
        }
    }
    ?>
    <script src="js/accordion.js"></script>
</body>
</html>